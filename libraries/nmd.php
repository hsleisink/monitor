<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */


	class NMD extends HTTPS {
		const SERVER = "www.notifymydevice.com";

		private $api_key = null;

		/* Constructor
		 *
		 * INPUT:  string API key
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($api_key) {
			$this->api_key = $api_key;

			parent::__construct(self::SERVER);
		}

		/* Send push notification
		 *
		 * INPUT:  string title, string text
		 * OUTPUT: true sending successful
		 * ERROR:  false sending failed
		 */
		public function send_notification($title, $text) {
			$data = array(
				"ApiKey"    => $this->api_key,
				"PushTitle" => $title,
				"PushText"  => $text);

			if (($result = $this->POST("/push", $data)) === false) {
				return null;
			} else if ($result["status"] != 200) {
				return false;
			}

			return true;
		}
	}
?>
