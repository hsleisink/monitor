<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
//
//  Filter template
//
//-->
<xsl:template match="filter">
<div class="filter">
<form action="{/output/page/@url}" method="post">
<xsl:if test="hours">
<span>Hours: <select class="filter hour" name="hour_from" onChange="javascript:submit()">
<xsl:for-each select="hours/hour">
	<xsl:if test=".&lt;=../@to">
	<option>
		<xsl:if test=".=../@from"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
		<xsl:value-of select="." />
	</option>
	</xsl:if>
</xsl:for-each>
</select> - <select class="filter hostname" name="hour_to" onChange="javascript:submit()">
<xsl:for-each select="hours/hour">
	<xsl:if test=".>=../@from">
	<option>
		<xsl:if test=".=../@to"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
		<xsl:value-of select="." />
	</option>
	</xsl:if>
</xsl:for-each>
</select></span>
</xsl:if>

<xsl:if test="hostnames">
<span>Host: <select class="filter webserver" name="hostname" onChange="javascript:submit()">
<xsl:for-each select="hostnames/hostname">
	<option value="{@id}">
		<xsl:if test="@selected='yes'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
		<xsl:value-of select="." />
	</option>
</xsl:for-each>
</select></span>
</xsl:if>

<span>Webserver: <select class="filter" name="webserver" onChange="javascript:submit()">
<xsl:for-each select="webservers/webserver">
	<option value="{@id}">
		<xsl:if test="@selected='yes'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
		<xsl:value-of select="." />
	</option>
</xsl:for-each>
</select></span>

<input type="hidden" name="submit_button" value="filter" />
</form>
</div>
</xsl:template>

</xsl:stylesheet>
